var Canvas = require('canvas');
var Util = require('util');
var Image = Canvas.Image;

var DEFAULTS = {
    colorBg: '999',
    colorFg: 'fff',
    maxFontSize: 400,
    maxHeight: 1600,
    maxWidth: 1600,
    textPadding: 0.1,
    rotate: false,
    logo: null
};

/**
 * tests strings for hexcode formatting
 * @param  {String} color color string to be tested
 * @return {Boolean}
 */
function testHex(color) {
    var isHex = false;

    if (/^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/.test(color)) {
        isHex = true;
    }

    return isHex;
}

/**
 * Breaks up request path in to individual tokens and
 * parses it for values that can be used in Artxy,
 * matching the first value found and discarding all
 * other non-matching tokens.
 * @param  {Object} req request object from Express.js
 * @return {Object}     JSON object containing values
 *                           that can be used in Artxy
 */
function tokenizePath(req) {
    var pathTokens = req.path.split('/');
    var widthReg = /((^\d+$)|(^\d+x\d+$))/;
    var paddingReg = /((^0\.0$)|(^0\.[0-4]{1}\d*$))/;
    var fgReg = /^fg:(.*)/i;
    var bgReg = /^bg:(.*)/i;
    var rotateReg = /^rotate:(.*)/;
    var rgbRegex = /(^rgb\((\d+),\s*(\d+),\s*(\d+)\)$)|(^rgba\((\d+),\s*(\d+),\s*(\d+)(,\s*\d+\.\d+)*\)$)/;
    var hexRegex = /^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/;
    var tokens = {
        "width": null,
        "height": null,
        "textPadding": null,
        "text": null,
        "colorBg": null,
        "colorFg": null,
        "rotate": null
    };

    for (var i in pathTokens) {
        // Iterate through path tokens to find matching values
        // If none is found the text value is set to the current token
        var match = false;

        // Test for width and height value(s)
        if (widthReg.test(pathTokens[i]) && !tokens.width) {
            var widthTokens = pathTokens[i].split('x');
            tokens.width = Math.min(widthTokens[0], maxWidth) || 300;
            if (tokens.width === 0) {
                tokens.width = 300;
            }
            tokens.height = Math.min(widthTokens[1], maxHeight) || tokens.width;
            if (tokens.height === 0) {
                tokens.height = tokens.width;
            }
            match = true;
        }

        // Test for text padding value
        if (paddingReg.test(pathTokens[i]) && !tokens.textPadding) {
            tokens.textPadding = pathTokens[i];
            match = true;
        }

        // Test for foreground colour value
        if (fgReg.test(pathTokens[i]) && !tokens.colorFg) {
            var fgColorVal = fgReg.exec(pathTokens[i])[1];
            if (rgbRegex.test(fgColorVal)) {
                tokens.colorFg = fgColorVal;
                match = true;
            } else if (hexRegex.test(fgColorVal)) {
                tokens.colorFg = fgColorVal;
                match = true;
            }
        }

        // Test for background colour value
        if (bgReg.test(pathTokens[i]) && !tokens.colorBg) {
            var bgColorVal = bgReg.exec(pathTokens[i])[1];
            if (rgbRegex.test(bgColorVal)) {
                tokens.colorBg = bgColorVal;
                match = true;
            } else if (hexRegex.test(bgColorVal)) {
                tokens.colorBg = bgColorVal;
                match = true;
            }
        }
        // Test for text rotation value
        if (rotateReg.test(pathTokens[i]) && !tokens.rotate) {
            var rotateVal = rotateReg.exec(pathTokens[i])[1];
            switch (rotateVal) {
                case "0":
                    tokens.rotate = false;
                    match = true;
                    break;
                case "false":
                    tokens.rotate = false;
                    match = true;
                    break;
                case "1":
                    tokens.rotate = true;
                    match = true;
                    break;
                case "true":
                    tokens.rotate = true;
                    match = true;
                    break;
            }
        }

        // Assign non-matching token to text if not already set
        if (match === false && !tokens.text) {
            tokens.text = unescape(pathTokens[i]);
        }
    }

    return tokens;
}

module.exports = function(options) {
    // Merge options passed in to function with global options
    var opts = {};

    for (var i in options) {
        opts[i] = {
            value: options[i],
            enumerable: true,
            writeable: true,
            configurable: true
        };
    }

    var config = Object.create(DEFAULTS, opts);

    for (var o in config) {
        this[o] = config[o];
    }

    /**
     * Create a PNG stream based on tokens passed via the
     * web server request. Defaults set at the time of loading
     * the middleware (or by the applicaiton itself) are used
     * as fallbacks if no matching token is found
     * @param  {Object}   req  request object from Express.js
     * @param  {Object}   res  response object from Express.js
     * @param  {Function} next 
     * @return {null}
     */
    return function(req, res, next) {

        // Get tokens from web request
        var pathTokens = tokenizePath(req);

        // Assign defaults
        var width = pathTokens.width || 300;
        var height = pathTokens.height || width;
        var textLineLength = width;
        var textWidthPercent = 1 - (pathTokens.textPadding || textPadding) * 2;
        var text = pathTokens.text || Util.format('%sx%s', width, height);
        var canvasBg = pathTokens.colorBg || colorBg;
        var canvasFg = pathTokens.colorFg || colorFg;
        var rotateText = pathTokens.rotate || rotate;

        // Create canvas
        var canvas = new Canvas(width, height);
        var ctx = canvas.getContext('2d');

        // Set background
        if (testHex(canvasBg)) {
            ctx.fillStyle = '#' + canvasBg;
        } else {
            ctx.fillStyle = canvasBg;
        }
        ctx.fillRect(0, 0, width, height);

        // If a logo is provided, load scaled to 80% of the shortest length
        // 
        // TODO: Add an option to disable the logo if it's not required,
        // and set the alpha via request tokens or as a default option
        if (logo) {
            var logoImage = new Image();

            logoImage.onload = function() {
                var logoWidth = logoImage.width;
                var logoHeight = logoImage.height;
                var logoToCanvasScale = 0;
                if (width >= height) {
                    logoToCanvasScale = (height / logoHeight) * 0.8;
                } else {
                    logoToCanvasScale = (width / logoWidth) * 0.8;
                }

                var logoPositionX = (width / logoToCanvasScale / 2) - (logoWidth / 2);
                var logoPositionY = (height / logoToCanvasScale / 2) - (logoHeight / 2);

                ctx.globalAlpha = 0.3;
                ctx.scale(logoToCanvasScale, logoToCanvasScale);
                ctx.drawImage(logoImage, logoPositionX, logoPositionY);
                ctx.globalAlpha = 1;
                ctx.scale(1 / logoToCanvasScale, 1 / logoToCanvasScale);
            };

            // File must be local due to cross-domain policy security.
            // If no file is found, the onload will not process.
            logoImage.src = __dirname + '/' + logo;
        }

        // Start of text rendering

        // Rotate canvas for text rendering
        // 
        // TODO: Rotate the canvas back to normal if additional
        // canvas elements need to be rendered after (on top of)
        // the text.
        if (rotateText === true) {
            textLineLength = Math.sqrt(width * width + height * height);
            var angleInRadians = Math.atan2(height, width);

            ctx.translate(width / 2, height / 2);
            ctx.rotate(-angleInRadians);
            ctx.translate(-width / 2, -height / 2);
        }

        // Find a rough estimate of the font size required to be equal
        // to or less than the maximum text width (with 1px set as the
        // minimum font size).
        var textFits = false;
        var fontSize = maxFontSize;
        while (!textFits) {
            ctx.font = Util.format('600 %spx Sans Serif', fontSize);
            var textMetrics = ctx.measureText(text);
            if (textMetrics.width > (textLineLength * textWidthPercent)) {
                fontSize *= 0.9;
                if (fontSize <= 1) {
                    fontSize = 1;
                    textFits = true;
                }
            } else {
                textFits = true;
            }
        }

        // Set the text in the center middle of the canvas
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        if (testHex(canvasFg)) {
            ctx.fillStyle = '#' + canvasFg;
        } else {
            ctx.fillStyle = canvasFg;
        }
        ctx.fillText(text, width / 2, height / 2);

        // Send canvas stream to the response
        res.header('Content-Type', 'image/png');
        var stream = canvas.pngStream();
        stream.pipe(res);
    };
};

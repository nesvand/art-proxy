# artxy

Dynamic PNG image placeholder generator using Node and Express. Taking inspiration from sites like [Placehold.it](http://placehold.it/) and originally forked from [imgipsum](https://github.com/jico/imgipsum), this personal research project has taken a life of its own.

Artxy stands for "Art Proxy", which is exactly what this application is intended for. Whether you require subtly branded placeholders, or something that stands out on a web site draft, Artxy has you covered.

## Requirements

* Node.js >= 0.10.0
* [Express](http://expressjs.com/) >= 4.0
* [Cairo](http://cairographics.org/) (required by [node-canvas](https://github.com/Automattic/node-canvas))

## Installation

Download `artxy` and save it as a dependency.

```bash
$ npm install artxy --save
```

Use it as middleware in your Express app.

```javascript
var express = require('express');
var app     = express();

var artxy = require('artxy');

app.use('/artxy', artxy({
    // Default overrides
    // ie.
    colorBg: 'f00',
    rotate: 1,
    logo: './logo.png'
}));

app.listen(8080);
console.log('Listening on ' + 8080);
```

### Settings

Artxy can be loaded with alternative image defaults using the following parameters (default values are listed)

```javascript
app.use(artxy({
  colorBg:     '999',      // Default image background color - supports hexcode, rgb and rgba
  colorFg:     'fff',      // Default image text color - supports hexcode, rgb and rgba
  maxFontSize: 400,        // Max font size
  maxHeight:   1600,       // Max image height
  maxWidth:    1600,       // Max image width
  textPadding: 0.1,        // Text padding (left/right) in percentage
  rotate:      false,      // Display the text diagonally across the image (bottom to top)
  logo:        null        // Background logo display - Image file location relative to artxy
                           // (ie. './logo.png') and it will be scaled to 80% of shortest side
                           // (for best quality, use an image matching your maxHeight/maxWidth)
}));
```

## Usage

The artxy middleware application has been designed to be bound at a route of your choosing when invoked by Express. As such, all options can be set by tokens found in the url after the set route.

Other than the fallback defaults mentioned above, the application will create a 300x300 image if no width is specified, and will reduce the font size to a minimum of 1px.

The first matching token will be used for the following options, and the first non-matching token will be used as the text for the image.

### Options
#### Width and Height
* The first positive number from 1 to maxWidth will be used as the width of the image (ie. `/400/` will create a 400 wide square)
* Using two positive numbers separated be `x` will set them as the width and height (ie. `/400x700/` will create a 400 wide and 700 high oblong)

#### Color
* Set the foreground colour by placing `fg:` in front of a long or short hexcode color, or an rgb/rgba color (ie. `fg:f00`, `fg:ededed`, `fg:rgb(200,150,100)` etc.)
* Note, do not include the `#` for the hexcode color


#### Text Rotation
* Use the key word `rotate:` in front of a boolean value - either `1` or `true` and either `0` or `false`

#### Text Padding
* Use a float value from 0 to 0.5 (inclusive of zero, represented by `0.0`). This will act as a "percent" of the width to be used as padding on the sides of the text (note, due to the rough calculation of font size, this is not an exact padding value)

#### Text
* The first token found that does not match any of the above options will be used as the text for the image, with a fallback to displaying the width and height in the form `widthxheight` (ie. "400x700"). Also note, the string found will be 'unescaped' before rendering. As such you can generate an image without text by including using a text token of `/%20/`.


### Examples

Generate the default 300x300 image using global settings:

`http://localhost:8080/artxy`

Generate a 600px square image using the global settings:

`http://localhost:8080/artxy/600`

Generate a 600x800px image using the global settings:

`http://localhost:8080/artxy/600x800`

Enable text rotation and replace the text with "Example":

`http://localhost:8080/artxy/rotate:1/Example`

Change the background and foreground colors using hexcode:

`http://localhost:8080/artxy/bg:f00/fg:00FFFF`

Change the background and foreground colors using rgb and rgba:

`http://localhost:8080/artxy/bg:rgb(0,255,0)/fg:rgba(255,255,255,0.5)`

Put it all together!

`http://localhost:8080/artxy/600x400/rotate:1/Replace Me/bg:00f/fg:rgba(255,255,255,0.5)`

## TODO

* Refactor and optimise code
* Add more error handling
* See if `#` can be handled gracefully

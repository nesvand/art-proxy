var express = require('express');
var app = express();

var artxy = require('./index.js');

app.use('/artxy', artxy({
    colorBg: 'f00',
    colorFg: 'F9D12E',
    logo: './logo.png'
}));

app.listen(8080);
console.log('Listening on ' + 8080);
